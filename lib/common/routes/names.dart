class AppRoutes {
  // welcome page or on boarding
  static const INITIAL = '/';

  // application page
  static const APPLICATION = '/application';

  // sign in page
  static const SIGN_IN = '/sign-in';

  // register page
  static const REGISTER = '/register';

  // home page
  static const HOME_PAGE = '/home-page';

  // for settings of the app
  static const SETTINGS = '/settings';
}
