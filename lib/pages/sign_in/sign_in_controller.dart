import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ulearning_app/common/values/constant.dart';
import 'package:ulearning_app/common/widgets/flutter_toast.dart';
import 'package:ulearning_app/global.dart';
import 'package:ulearning_app/pages/sign_in/bloc/sign_in_blocs.dart';

class SignInController {
  final BuildContext context;

  const SignInController({required this.context});

  Future<void> handleSignIn(String type) async {
    try {
      if (type == 'email') {
        /**
         * TODO: context.read<SignInBloc>().state
         * context.read<SignInBloc>() trả về instance của SignInBloc
         * sau đó .state sẽ được gọi trên đối tượng đó để trả về
         * trạng thái hiện tại của Bloc
         *
         * TODO: Tương tự:
         * TODO: BlocProvider.of<SignInBloc>(context).state
         */
        final state = context.read<SignInBloc>().state;
        String emailAddress = state.email;
        String password = state.password;

        if (emailAddress.isEmpty) {
          //
          toastInfo(msg: 'Your need to fill in email address');
          return;
        }
        if (password.isEmpty) {
          //
          toastInfo(msg: 'Your need to fill in password');
          return;
        }
        try {
          final credential = await FirebaseAuth.instance
              .signInWithEmailAndPassword(
                  email: emailAddress, password: password);
          if (credential.user == null) {
            //
            toastInfo(msg: "You don't exist");
            return;
          }
          // ~ credential.user!.emailVerified != null
          if (!credential.user!.emailVerified) {
            //
            toastInfo(msg: 'You need to verify your email account');
            return;
          }

          var user = credential.user;
          if (user != null) {
            print('user exist');
            Global.storageService.setString(AppConstants.STORAGE_USER_TOKEN_KEY, '12345678');

            Navigator.of(context)
                .pushNamedAndRemoveUntil('/application', (route) => false);
            // we got verified user from firebase
          } else {
            toastInfo(msg: 'Currently you are not a user of this app');
            return;
            // we have error getting user from firebase
          }
        } on FirebaseAuthException catch (e) {
          if (e.code == 'user-not-found') {
            print('No user found for that email.');
            toastInfo(msg: 'No user found for that email');
            // toastInfo(msg: 'No user found for that email.');
          } else if (e.code == 'wrong-password') {
            print('Wrong password provided for that user.');
            toastInfo(msg: 'Wrong password provided for that user');
            // toastInfo(msg: 'Wrong password provided for that user.');
          } else if (e.code == 'invalid-email') {
            print('Your email format is wrong');
            toastInfo(msg: 'Your email format is wrong');
          }
        }
      }
    } catch (e) {}
  }
}
