import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ulearning_app/pages/sign_in/bloc/sign_in_events.dart';
import 'package:ulearning_app/pages/sign_in/bloc/sign_in_states.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState>{
  SignInBloc():super(SignInState()){
    on<EmailEvent>(_emailEvent);

    on<PasswordEvent>(_passwordEvent);
  }

  /**
   * TODO: Viết tường minh ra:
   * on<EmailEvent>(_emailEvent);
   *
   * TODO: của cái này:
   * on<EmailEvent>((event, emit) {
      emit(state.copyWith(email: event.email));
      });

      -> Như vậy, ta thấy emit là 1 instance của Emitter<SignInState>
      nhưng được viết dưới dạng 1 function
   */
  void _emailEvent(EmailEvent event, Emitter<SignInState> emit) {
    // print('my email is: ${event.email}');
    emit(state.copyWith(email: event.email));
  }

  void _passwordEvent(PasswordEvent event, Emitter<SignInState> emit) {
    // print('my password is: ${event.password}');
    emit(state.copyWith(password: event.password));
  }
}