import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:ulearning_app/pages/register/bloc/register_blocs.dart';
import 'package:ulearning_app/pages/register/bloc/register_events.dart';
import 'package:ulearning_app/pages/register/bloc/register_states.dart';
import 'package:ulearning_app/pages/register/register_controller.dart';

import '../common_widgets.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<RegisterBloc, RegisterStates>(builder: (context, state) {
      return Container(
        color: Colors.white,
        child: SafeArea(
          child: Scaffold(
            backgroundColor: Colors.white,
            appBar: buildAppBar('Sign up'),
            /**
             * I want to avoid overflow errors in case the content
             * inside a Column goes off screen.
             */
            body: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(
                    height: 20.w,
                  ),
                  reusableText('Enter your details below and free sign up'),
                  Container(
                    margin: EdgeInsets.only(top: 60.h),
                    padding: EdgeInsets.only(left: 25.w, right: 25.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        reusableText('User name'),
                        buildTextField('Enter your user name', 'name', 'user',
                            (value) {
                          context
                              .read<RegisterBloc>()
                              .add(UserNameEvent(value));
                        }),
                        reusableText('Email'),
                        buildTextField(
                            'Enter your email address', 'email', 'user',
                            (value) {
                          context.read<RegisterBloc>().add(EmailEvent(value));
                        }),
                        reusableText('Password'),
                        buildTextField(
                            'Enter your Password', 'password', 'lock', (value) {
                          context
                              .read<RegisterBloc>()
                              .add(PasswordEvent(value));
                        }),
                        reusableText('Re-enter password'),
                        buildTextField('Re-enter your password to confirm',
                            'password', 'lock', (value) {
                          context
                              .read<RegisterBloc>()
                              .add(RePasswordEvent(value));
                        }),
                        reusableText(
                            'By creating an account you have to agree with our them & condition.'),
                        buildLogInAndRegButton('Sign Up', 'login', () {
                          // Navigator.of(context).pushNamed('/register');
                          RegisterController(context: context).handleEmailRegister();
                        }),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    });
  }
}
