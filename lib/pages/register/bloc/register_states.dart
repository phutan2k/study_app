class RegisterStates {
  final String userName;
  final String email;
  final String password;
  final String rePassword;

  /**
   * TODO: Trong constructor, cặp dấu {} là optional named parameter
   * đối với các thuộc tính được khai báo là final,
   * ta có thể sử dụng required để khai báo trong constructor
   * hoặc
   * gán default value cho chúng
   */
  const RegisterStates({
    this.userName = '',
    this.email = '',
    this.password = '',
    this.rePassword = '',
  });

  RegisterStates copyWith({
    String? userName,
    String? email,
    String? password,
    String? rePassword,
  }) {
    return RegisterStates(
      userName: userName ?? this.userName,
      email: email ?? this.email,
      password: password ?? this.password,
      rePassword: rePassword ?? this.rePassword,
    );
  }
}
