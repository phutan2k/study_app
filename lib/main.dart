import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:ulearning_app/common/routes/routes.dart';
import 'package:ulearning_app/common/values/colors.dart';
import 'package:ulearning_app/global.dart';

import 'firebase_options.dart';

Future<void> main() async {
  await Global.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [...AppPages.allBlocProviders(context)],
      child: ScreenUtilInit(
        /**
         * để đại diện cho kích thước của thiết kế gốc.
         * Flutter sử dụng thông tin này để tính toán tỷ lệ chuyển đổi
         * khi hiển thị giao diện trên mọi thiết bị.
         */
        designSize: Size(375, 812),
        builder: (context, child) => MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
              appBarTheme: AppBarTheme(
            iconTheme: IconThemeData(
              color: AppColors.primaryText,
            ),
            elevation: 0,
            backgroundColor: Colors.white,
          )),
          onGenerateRoute: AppPages.generateRouteSettings,
        ),
      ),
    );
  }
}
